import uuid
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    reagent_user = models.ForeignKey("User", on_delete=models.SET_NULL, blank=True, null=True,
                                     related_name="reagent_user_set", verbose_name=_('reagent user'))
    reagent_code = models.CharField(max_length=5, verbose_name=_("reagent code"), blank=True, null=True, unique=True)
    presented_users = models.ManyToManyField("User", blank=True, verbose_name=_('reagent list'),
                                             related_name="presented_users_set")

    def __str__(self):
        return str(self.username)


@receiver(post_save, sender=User)
def set_reagent_code(sender, instance, created, **kwargs):
    flag = True
    if created:
        while flag:
            code = uuid.uuid4().hex[:8].upper()
            user_obj = User.objects.filter(reagent_code__exact=code).first()
            if user_obj:
                continue
            else:
                flag = False

        instance.reagent_code = code
        instance.save()
