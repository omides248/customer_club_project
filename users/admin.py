from django.contrib import admin
# Register your models here.
from django.contrib.auth.admin import UserAdmin
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from users.models import User


@admin.register(User)
class UserAdmin(UserAdmin):

    def __init__(self, model, admin_site):
        self.request = None
        super().__init__(model, admin_site)

    def get_queryset(self, request):
        self.request = request
        return super().get_queryset(request)

    """Define admin model for custom User model with no email field."""

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'),
         {'fields': ('username', 'first_name', 'last_name', 'reagent_code', 'presented_users'
                     )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Presented Users'), {'fields': ('reagent_user', 'presented_users_table',)}),

    )
    filter_horizontal = ('presented_users',)

    list_display = ('username', 'first_name', 'last_name', 'is_active', 'is_staff', 'is_superuser',)
    search_fields = (
        'username', 'first_name', 'last_name', 'is_active', 'is_staff', 'is_superuser',)
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2'),
        }),
    )

    readonly_fields = ('presented_users_table',)

    def presented_users_table(self, instance):

        ip = self.request.META['HTTP_HOST']
        protocol = 'https://'
        if not self.request.is_secure():
            protocol = 'http://'

        style_table = "border:1px solid #eee; border-collapse:collapse; width: 100%;"
        style_th_td = "font-size: 16px;border: 1px solid #ddd; padding: 8px;"
        style_th = ";padding-top: 12px;padding-bottom: 12px;text-align: right;background-color: #79aec8;color: white;"
        style_tr = "background-color: #f2f2f2;"

        html = mark_safe(
            f"<table style='{style_table}'><tr>"
            f"<th style='{style_th_td}{style_th}'>نام</th>"
            f"<th style='{style_th_td}{style_th}'>نام خانوادگی</th>"
            f"<th style='{style_th_td}{style_th}'>نام کاربری</th></tr>"
        )
        presented_user_objs = instance.presented_users.all()
        if presented_user_objs:
            for key, user in enumerate(presented_user_objs):
                if key % 2 == 0:
                    html += mark_safe(
                        f"<tr style='{style_tr}'><td style='{style_th_td}'>"
                        + "<a href=" + protocol + ip + "/admin/users/user/{}/change/>".format(user.pk) + user.first_name + "</a>"
                        + f"</td><td style='{style_th_td}'>"
                        + user.last_name + f"</td><td style='{style_th_td}'>"
                        + user.username + "</td></tr>")
                else:
                    html += mark_safe(
                        f"<tr><td style='{style_th_td}'>"
                        + "<a href=" + protocol + ip + "/admin/users/user/{}/change/>".format(
                            user.pk) + user.first_name + "</a>"
                        + f"</td><td style='{style_th_td}'>"
                        + user.last_name + f"</td><td style='{style_th_td}'>"
                        + user.username + "</td></tr>")

            html += mark_safe("</table>")
            return html
        else:
            return mark_safe("<span>این مشتری هنوز کاربری را معرفی نکرده است</span>")

    presented_users_table.short_description = "Presented user"
